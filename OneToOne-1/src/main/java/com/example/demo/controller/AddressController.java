package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Address;
import com.example.demo.repo.AddressRepository;

@RestController
public class AddressController {
    
	@Autowired
    private AddressRepository addressRepository;
    
	@GetMapping("/address/get/all")
    public List<Address> getAddress() {
        return addressRepository.findAll();
    }
}
