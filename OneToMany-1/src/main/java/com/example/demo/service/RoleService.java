package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Role;
import com.example.demo.repo.RoleRepository;
//import com.example.demo.repo.UserRepository;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;

	//@Autowired
	//private UserRepository userRepository;


	    @Transactional
	    public ResponseEntity<Object> addRole(Role role) {

	        Role newRole = new Role();
	        newRole.setName(role.getName());
	        newRole.setDescription(role.getDescription());

	        newRole.setUsers(role.getUsers());
	        Role savedRole = roleRepository.save(newRole);
	        if (roleRepository.findById(savedRole.getId()).isPresent()) {
	            return ResponseEntity.accepted().body("Successfully Created Role and Users");
	        } else
	            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Role");
	    }

	   
	    public ResponseEntity<Object> deleteRole(Long id) {
	        if (roleRepository.findById(id).isPresent()) {
	            roleRepository.deleteById(id);
	            if (roleRepository.findById(id).isPresent()) {
	                return ResponseEntity.unprocessableEntity().body("Failed to delete the specified record");
	            } else return ResponseEntity.ok().body("Successfully deleted specified record");
	        } else
	            return ResponseEntity.unprocessableEntity().body("No Records Found");
	    }
}
